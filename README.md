# Especificação do Teste

* Utilizado spring-boot com banco H2 e framework de persistencia mybatis.

## Como rodar
* mvn spring-boot:run
* Acesso localhost:8080/api/

### APIs

* Requisito da API
* Retorna os dois primeiros produtores com menor intervalo entre premiações e os dois primeiros com o maior intervalor entre premiações.
* Filmes com mais de um produtor são distribuídos para cada produtor exclusivamente

```shell
curl --request GET \
  --url http://localhost:8080/api/movies/winners-min-max-interval
```

* Listar todos filmes

```shell
curl --request GET \   --url http://localhost:8080/api/movies
```

* Filtrar filmes

```shell
curl --request POST \
  --url http://localhost:8080/api/moviesBy \
  --header 'Content-Type: application/json' \
  --data '{
	"year": 1980,
	"title": "Can'\''t Stop the Music",
	"studios": "Associated Film Distribution",
	"producers": "Allan Carr",
	"winner": "yes"
}'
```

* Atualizar filme

```shell
curl --request PUT \
  --url http://localhost:8080/api/movie/modify \
  --header 'Content-Type: application/json' \
  --data '{"title": "Can'\''t Stop the Music", "year": 9999, "studios":"a4a5sfd46sadfasfda"}'
```

* Excluir filme

```shell
curl --request POST \
  --url http://localhost:8080/api/movie/remove \
  --header 'Content-Type: application/json' \
  --data '{"title": "Can'\''t Stop the Music"}'
```

# Api de cadastro de contato e telefone
* Utilizei um projeto de outros testes que vinha fazendo
* Se for de interesse funcionalidade pode ser testada no endereço http://3.16.188.7:8080/webapp/
* O front-end é com jsf que não está nesse repositório, mas a persistência via banco utiliza essa mesma api

Alguns dos endpoints

* Busca clientes

```shell
curl --request GET \
  --url http://localhost:8080/api/clientes/
```

* Adicionar cliente

```shell
curl --request POST \
  --url http://localhost:8080/api/cliente/add \
  --header 'Content-Type: application/json' \
  --data '{
	"nr_cpf_cliente": 32165498700,
	"nm_cliente": "launch-wizard-1",
	"ds_email_cliente": "mail@mail.com",
	"dt_atualizacao": "2021-08-02T12:35:44.852+0000"
}'
```

