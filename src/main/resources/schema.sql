--movieaward
create table movieaward
(
	year numeric(4),
	title text not null,
	studios text,
	producers text,
	winner text
);


--tabela cliente

create table cliente (
    cd_cliente numeric(6) not null,
    nr_cpf_cliente numeric(16),
    nm_cliente varchar(50),
    ds_email_cliente varchar(100),
    dt_atualizacao timestamp,
    constraint pk_cliente primary key(cd_cliente)
);

--tabela telefone_cliente

create table telefone_cliente(
    id_telefone numeric(6) not null,
    cd_cliente numeric(6) not null,
    cd_tipo_telefone numeric(6),
    nr_telefone varchar(60),
    dt_atualizacao timestamp,
    constraint pk_telefone_cliente primary key (id_telefone),
    constraint fk_cliente_telefone_cliente foreign key (cd_cliente) references cliente(cd_cliente)
);

--tabela tipo_telefone
create table tipo_telefone(
    cd_tipo_telefone numeric(6) not null,
    ds_tipo_telefone varchar(60),
    dt_atualizacao timestamp,
    constraint pk_tipo_telefone primary key (cd_tipo_telefone)
);
