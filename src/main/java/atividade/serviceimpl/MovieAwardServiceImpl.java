package atividade.serviceimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import atividade.IntervalComparator;
import atividade.MovieAwardComparator;
import atividade.mapper.MovieAwardMapper;
import atividade.model.MovieAward;
import atividade.model.WinnerInterval;
import atividade.service.MovieAwardService;

@Service
public class MovieAwardServiceImpl implements MovieAwardService {

	@Autowired
	private MovieAwardMapper movieAwardMapper;
	
	@Override
	public List<MovieAward> getMovies() {
		return movieAwardMapper.findAll();
	}

	@Override
	public List<MovieAward> getMoviesBy(MovieAward movieAward) {
		return movieAwardMapper.findBy(movieAward);
	}

	@Override
	public int createMovieAward(MovieAward movieAward) {
		if (StringUtils.isEmpty(movieAward.getWinner())) {
			movieAward.setWinner(MovieAward.NO);
		}
		return movieAwardMapper.insert(movieAward);
	}
	
	@Override
	public int updateMovieAward(MovieAward movieAward) {
		return movieAwardMapper.update(movieAward);
	}

	@Override
	public int deleteMovieAward(MovieAward movieAward) {
		return movieAwardMapper.delete(movieAward);
	}

	@Override
	public Map<String, List<WinnerInterval>> getWinnersMinMaxInterval() {
		MovieAward mAward = new MovieAward();
		mAward.setWinner(MovieAward.YES);
		List<MovieAward> movies = getMoviesBy(mAward);
		
		List<String> p = new ArrayList<String>();
				
		HashMap<String, List<MovieAward>> producers = new HashMap<String, List<MovieAward>>();
		
		for (MovieAward movieAward : movies) {
			p.clear();
			String[] split1 = movieAward.getProducers().replaceAll(" and ", ",").replaceAll(",,",",").split(",");
		    Collections.addAll(p, split1);
			for (String  s: p) {
				String strim = s.trim();
				List<MovieAward> list = producers.get(strim);
				if (list == null) {
					list = new ArrayList<MovieAward>();
				}
				list.add(movieAward);
				producers.put(strim, list);
			}
		}
		
		WinnerInterval minWinnerInterval, maxWinnerInterval;
		List<WinnerInterval> minList = new ArrayList<WinnerInterval>();
		List<WinnerInterval> maxList = new ArrayList<WinnerInterval>();
		
		for (Iterator<Entry<String, List<MovieAward>>> iterator = producers.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, List<MovieAward>> entry = iterator.next();
			List<MovieAward> value = entry.getValue();
			int size = value.size(), minInterval = 0, maxInterval = 0;
			if (size > 1) {
				Collections.sort(value, new MovieAwardComparator());
				for (int i = 0; i < size; i++) {
					try {
						int calc = Math.abs(value.get(i).getYear() - value.get(i+1).getYear());
						if (i == 0 || calc < minInterval) {
							minInterval = calc;
							minWinnerInterval = new WinnerInterval();
							minWinnerInterval.setPreviousWin(value.get(i).getYear());
							minWinnerInterval.setFollowingWin(value.get(i+1).getYear());
							minWinnerInterval.setInterval(minInterval);
							minWinnerInterval.setProducer(entry.getKey());
							minList.add(minWinnerInterval);
						}
						if (calc > maxInterval) {
							maxInterval = calc;
							maxWinnerInterval = new WinnerInterval();
							maxWinnerInterval.setPreviousWin(value.get(i).getYear());
							maxWinnerInterval.setFollowingWin(value.get(i+1).getYear());
							maxWinnerInterval.setInterval(maxInterval);
							maxWinnerInterval.setProducer(entry.getKey());
							maxList.add(maxWinnerInterval);
						}
					} catch (IndexOutOfBoundsException e) {
						continue;
					}
				}
			}
		}
		Collections.sort(minList, new IntervalComparator());
		Collections.sort(maxList, new IntervalComparator());
		Map<String, List<WinnerInterval>> map = new HashMap<String, List<WinnerInterval>>();
		
		createMap(map, true, minList);
		createMap(map, false, maxList);
		
		return map;
	}
	
	
	private Map<String, List<WinnerInterval>> createMap(Map<String, List<WinnerInterval>> map, boolean min, List<WinnerInterval> list) {
		int size = list.size();
		List<WinnerInterval> intervalList = new ArrayList<WinnerInterval>();
		if (size > 0) {
			if (min) {
				for (int i = 0; i < size; i++) {
					if (i > 0) {
						break;
					}
					intervalList.add(list.get(i));
				}
				map.put("min", intervalList);
			} else {
				int count = 0;
				for (int i = size-1; i >= 0; i--) {
					if (count++ > 0) {
						break;
					}
					intervalList.add(list.get(i));
				}
				map.put("max", intervalList);
			}
		}
		return map;
	}
	
	
}
