package atividade.service;

import java.util.List;
import java.util.Map;

import atividade.model.MovieAward;
import atividade.model.WinnerInterval;

public interface MovieAwardService {

	
	List<MovieAward> getMovies();
	
	List<MovieAward> getMoviesBy(MovieAward movieAward);
	
	int createMovieAward(final MovieAward movieAward);
	
	int updateMovieAward(final MovieAward movieAward);
	
	int deleteMovieAward(final MovieAward movieAward);
	
	Map<String, List<WinnerInterval>> getWinnersMinMaxInterval();
	
	
}
