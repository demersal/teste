package atividade.model;


import lombok.Data;

@Data
public class MovieAward {
	
	public static final String YES = "yes";
	public static final String NO = "no";

	private Integer year;
	private String title;
	private String studios;
	private String producers;
	private String winner;
		
}
