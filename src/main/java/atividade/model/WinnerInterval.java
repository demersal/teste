package atividade.model;

import lombok.Data;

@Data
public class WinnerInterval {
	
	private String producer;
	private Integer interval;
	private Integer previousWin;
	private Integer followingWin;

}
