package atividade;

import java.util.Comparator;

import atividade.model.WinnerInterval;

public class IntervalComparator implements Comparator<WinnerInterval> {

	@Override
	public int compare(WinnerInterval o1, WinnerInterval o2) {
		return o1.getInterval() < o2.getInterval() ? -1 : o1.getInterval() > o2.getInterval() ? 1 : 0;
	}

}
