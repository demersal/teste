package atividade.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import atividade.model.MovieAward;
import atividade.model.WinnerInterval;
import atividade.service.MovieAwardService;

@RestController
public class MovieAwardController {
	
	@Autowired
	private MovieAwardService movieAwardService;
	
	@PostMapping("/movie/add")
    public int createMovieAward(@RequestBody final MovieAward movieAward) {
        return movieAwardService.createMovieAward(movieAward);
    }
	
	@PutMapping("/movie/modify")
	public int updateMovieAward(@RequestBody final MovieAward movieAward) {
		return movieAwardService.updateMovieAward(movieAward);
	}
	
	@PostMapping("/movie/remove")
	public int deleteMovieAward(@RequestBody final MovieAward movieAward) {
		return movieAwardService.deleteMovieAward(movieAward);
	}
	
	@GetMapping("/movies")
	public List<MovieAward> findAll() {
		return movieAwardService.getMovies();
	}
	
	@PostMapping("/moviesBy")
	public List<MovieAward> findAllBy(@RequestBody final MovieAward movieAward) {
		return movieAwardService.getMoviesBy(movieAward);
	}
	
	@GetMapping("/movies/winners-min-max-interval")
	public Map<String, List<WinnerInterval>> getWinnersMinMaxInterval() {
		return movieAwardService.getWinnersMinMaxInterval();
	}
	

}
