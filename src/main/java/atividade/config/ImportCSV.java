package atividade.config;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import atividade.model.MovieAward;
import atividade.service.MovieAwardService;

@Component
public class ImportCSV implements InitializingBean {

	List<MovieAward> list;

	@Autowired
	MovieAwardService movieAwardService;

	@Override
	public void afterPropertiesSet() throws Exception {

		File csvFile = new File(Thread.currentThread().getContextClassLoader().getResource("movielist.csv").getPath());
		
		CsvMapper mapper = new CsvMapper();
		CsvSchema csvSchema = mapper.typedSchemaFor(MovieAward.class).withHeader().withColumnSeparator(';')
				.withColumnReordering(true);
		MappingIterator<MovieAward> it = mapper.readerWithTypedSchemaFor(MovieAward.class).with(csvSchema)
				.readValues(csvFile);

		list = it.readAll();
		readCSV();
	}

	private void readCSV() throws Exception {
		for (MovieAward movie : list) {
			movieAwardService.createMovieAward(movie);
		}
	}
	
	public static void main(String[] args) {
		String s = "lla and opp";
		String[] split = s.split(" and ");
		for (String string : split) {
			System.out.println(string);
		}
		
	}

}
