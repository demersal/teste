package atividade;

import java.util.Comparator;

import atividade.model.MovieAward;

public class MovieAwardComparator implements Comparator<MovieAward> {

	@Override
	public int compare(MovieAward o1, MovieAward o2) {
		return o1.getYear() < o2.getYear() ? -1 : o1.getYear() > o2.getYear() ? 1 : 0;
	}

}
