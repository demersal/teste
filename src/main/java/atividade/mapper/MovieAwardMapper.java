package atividade.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import atividade.model.MovieAward;

@Mapper
public interface MovieAwardMapper {

	List<MovieAward> findAll();
	
	int insert(MovieAward movieAward);
	
	List<MovieAward> findBy(MovieAward movieAward);
	
	int update(MovieAward movieAward);
	
	int delete(MovieAward movieAward);
}
